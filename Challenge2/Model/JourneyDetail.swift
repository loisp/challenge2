//
//  JourneyDetail.swift
//  Challenge2
//
//  Created by Lois Pangestu on 13/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import Foundation
import UIKit

struct JourneyDetail {
    var image: UIImage
    var title: String
    var description: String
    var type: String
    var category: String
    var duration: String
    var goals: [String]
    var constraints: [String]
    var deliverables: [String]
}


class JourneyDetailManager {
    var arrayOfJourneyDetails: [JourneyDetail]
    init() {
        arrayOfJourneyDetails = [
        JourneyDetail(image: UIImage(named: "Challenge/challenge1")!,
                      title: "Challenge 1",
                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
                      type: "MINI",
                      category: "GROUP",
                      duration: "5",
                      goals: ["Full Cycle CBL", "Human Interface Guidelines", "Prototyping", "XCode & Basic Swift", "Group Collaboration"],
                      constraints: ["No Game", "No Back Ends", "No Augmented Reality"],
                      deliverables: ["Working Prototype"]),
      JourneyDetail(image: UIImage(named: "Challenge/challenge2")!,
                    title: "Challenge 2",
                    description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
                    type: "NANO",
                    category: "INDIVIDUAL",
                    duration: "2",
                    goals: ["Applied Investigation", "Everyone Can Code"],
                    constraints: ["This is Individual Work", "No Third Party Library", "Use HIG Interfaces Essentials"],
                    deliverables: ["Sketch Prototype", "Learning Backlog Document", "Xcode Project", "Feedback Report Document"]),
        JourneyDetail(image: UIImage(named: "Challenge/challenge3")!,
                      title: "Challenge 3",
                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
                      type: "MINI",
                      category: "GROUP",
                      duration: "4",
                      goals: ["Full Cycle CBL", "Human Interface Guidelines", "Prototyping", "XCode & Basic Swift", "Group Collaboration"],
                      constraints: ["No Game", "No Back Ends", "No Augmented Reality"],
                      deliverables: ["Working Prototype"]),
        JourneyDetail(image: UIImage(named: "Challenge/challenge4")!,
                      title: "Challenge 4",
                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
                      type: "MINI",
                      category: "GROUP",
                      duration: "4",
                      goals: ["To Be Announced…"],
                      constraints: ["To Be Announced…"],
                      deliverables: ["To Be Announced…"]),
        JourneyDetail(image: UIImage(named: "Challenge/challenge5")!,
                      title: "Challenge 5",
                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
                      type: "NANO",
                      category: "INDIVIDUAL",
                      duration: "2",
                      goals: ["To Be Announced…"],
                      constraints: ["To Be Announced…"],
                      deliverables: ["To Be Announced…"]),
        JourneyDetail(image: UIImage(named: "Challenge/challenge6")!,
                      title: "Challenge 6",
                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
                      type: "MACRO",
                      category: "GROUP",
                      duration: "12",
                      goals: ["To Be Announced…"],
                      constraints: ["To Be Announced…"],
                      deliverables: ["To Be Announced…"])
        ]
    }
}
