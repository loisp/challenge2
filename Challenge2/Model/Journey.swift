//
//  Journey.swift
//  Challenge2
//
//  Created by Lois Pangestu on 12/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

struct Journey {
    
    // For List Journey
    var image: UIImage
    var title: String
    var category: String
    var duration: String
    
    init(image: UIImage, title: String, category:String, duration: String){
        self.image = image
        self.title = title
        self.category = category
        self.duration = duration
    }
}

//struct JourneyDetail {
//    var image: UIImage
//    var title: String
//    var description: String
//    var type: String
//    var category: String
//    var duration: String
//    var goals: [String]
//    var constraints: [String]
//    var deliverables: [String]
//}
//
//class JourneyDetailManager {
//    var arrayOfJourneyDetails: [JourneyDetail]
//    init() {
//        arrayOfJourneyDetails = [
//        JourneyDetail(image: JourneyImages.challenge1,
//                      title: "Challenge 1",
//                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
//                      type: "MINI",
//                      category: "GROUP",
//                      duration: "5",
//                      goals: ["Full Cycle CBL", "Human Interface Guidelines", "Prototyping", "XCode & Basic Swift", "Group Collaboration"],
//                      constraints: ["No Game", "No Back Ends", "No Augmented Reality"],
//                      deliverables: ["Working Prototype"]),
//      JourneyDetail(image: JourneyImages.challenge2,
//                    title: "Challenge 2",
//                    description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
//                    type: "NANO",
//                    category: "INDIVIDUAL",
//                    duration: "2",
//                    goals: ["Applied Investigation", "Everyone Can Code"],
//                    constraints: ["This is Individual Work", "No Third Party Library", "Use HIG Interfaces Essentials"],
//                    deliverables: ["Sketch Prototype", "Learning Backlog Document", "Xcode Project", "Feedback Report Document"]),
//        JourneyDetail(image: JourneyImages.challenge3,
//                      title: "Challenge 3",
//                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
//                      type: "MINI",
//                      category: "GROUP",
//                      duration: "4",
//                      goals: ["Full Cycle CBL", "Human Interface Guidelines", "Prototyping", "XCode & Basic Swift", "Group Collaboration"],
//                      constraints: ["No Game", "No Back Ends", "No Augmented Reality"],
//                      deliverables: ["Working Prototype"]),
//        JourneyDetail(image: JourneyImages.challenge4,
//                      title: "Challenge 4",
//                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
//                      type: "MINI",
//                      category: "GROUP",
//                      duration: "4",
//                      goals: ["To Be Announced…"],
//                      constraints: ["To Be Announced…"],
//                      deliverables: ["To Be Announced…"]),
//        JourneyDetail(image: JourneyImages.challenge5,
//                      title: "Challenge 5",
//                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
//                      type: "NANO",
//                      category: "INDIVIDUAL",
//                      duration: "2",
//                      goals: ["To Be Announced…"],
//                      constraints: ["To Be Announced…"],
//                      deliverables: ["To Be Announced…"]),
//        JourneyDetail(image: JourneyImages.challenge6,
//                      title: "Macro Challenge",
//                      description: "T-63 to Planet The Mini. To prepare for our first planet exploration, we need to explore iOS technology and create tools to aid us for the journet ahead. All explorers are expected to understand basic iOS development during this challenge.",
//                      type: "MACRO",
//                      category: "GROUP",
//                      duration: "12",
//                      goals: ["To Be Announced…"],
//                      constraints: ["To Be Announced…"],
//                      deliverables: ["To Be Announced…"]),
//        ]
//    }
//}

//class JourneyManager {
//    var arrayOfJourneys: [Journey]
//    init() {
//        arrayOfJourneys = [
//        Journey(image: JourneyImages.challenge1, title: "Challenge 1", category: "Group Challenge", duration: "5 Weeks"),
//        Journey(image: JourneyImages.challenge2, title: "Challenge 2", category: "Individual Challenge", duration: "2 Weeks"),
//        Journey(image: JourneyImages.challenge3, title: "Challenge 3", category: "Group Challenge", duration: "4 Weeks"),
//        Journey(image: JourneyImages.challenge4, title: "Challenge 4", category: "Group Challenge", duration: "4 Weeks"),
//        Journey(image: JourneyImages.challenge5, title: "Challenge 5", category: "Individual Challenge", duration: "2 Weeks"),
//        Journey(image: JourneyImages.challenge6, title: "Macro Challenge", category: "Group Challenge", duration: "12 Weeks")
//        ]
//    }
//}
