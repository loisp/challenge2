//
//  JuniorExplorer.swift
//  Challenge2
//
//  Created by Lois Pangestu on 15/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import Foundation
import UIKit

struct JuniorExplorer {
    
    var nameExpl : String
    var levelExplorer : String
    var shiftExpl : String
    var roleExpl : String
    var photoExpl : UIImage
    var wantToLearn : String!
    var canHelpWith : String!
}

class MorningExplorerManager {
    var arrayOfMorningExpl: [JuniorExplorer]
    
    init() {
        arrayOfMorningExpl = [
            JuniorExplorer(nameExpl: "Ahmad Rizki Maulana", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Rizki_MO")!, wantToLearn: "", canHelpWith: "Coding Basic"),
            JuniorExplorer(nameExpl: "Albert Pangestu", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Albert_MO")!, wantToLearn: "Ideation.\nSpeak English.\nUI and UX Design.", canHelpWith: "Programming Basics."),
            JuniorExplorer(nameExpl: "Alifudin Aziz", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Aziz_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Amelia Verina Siswanto", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Amelia_MO")!, wantToLearn: "Swift, UI/UX Design, Public Speaking, English Practice", canHelpWith: "Design, Interior & Architecture Design."),
            JuniorExplorer(nameExpl: "Angela Priscila Montolalu", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Angel_MO")!, wantToLearn: "UI/UX Design, Swift, English Speaking", canHelpWith: "Graphic Design (Branding)"),
            JuniorExplorer(nameExpl: "Ardy Stephanus", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Ardy_MO")!, wantToLearn: "", canHelpWith: "Basic Coding, Basic Swift"),
            JuniorExplorer(nameExpl: "Arifin Firdaus", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Arifin_MO")!, wantToLearn: "", canHelpWith: "iOS Development (UIKit), Clean Code"),
            JuniorExplorer(nameExpl: "Arnold Pangestu", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Arnold_MO")!, wantToLearn: "", canHelpWith: "Coding, Basic UI/UX Game Design, Unity;"),
            JuniorExplorer(nameExpl: "Audhy Virabri Kressa", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Audhy_MO")!, wantToLearn: "", canHelpWith: "- basic wift\n- git version controller\n- parsing data"),
            JuniorExplorer(nameExpl: "Baby Amelia Andina Putri", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Bussiness", photoExpl: UIImage(named: "Baby_MO")!, wantToLearn: "- Swift \n- xCode\n- UI/UX design", canHelpWith: "Business insights, public speaking and presentation skill, basic design."),
            JuniorExplorer(nameExpl: "Bagus Setiawan", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Bagus_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Benyamin Rondang Tuahta", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Ben_MO")!, wantToLearn: "Leading & trailing swipe action", canHelpWith: "Basic coding"),
            JuniorExplorer(nameExpl: "Dinie P Hemas", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Dinie_MO")!, wantToLearn: "Public speaking, English", canHelpWith: "Basic coding, UI pattern"),
            JuniorExplorer(nameExpl: "Dionesia Nadya Dewayani", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Nadya_MO")!, wantToLearn: "", canHelpWith: "Illustration"),
            JuniorExplorer(nameExpl: "Edward da Costa", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Edward_MO")!, wantToLearn: "Design , Advance Code", canHelpWith: "Basic Code"),
            JuniorExplorer(nameExpl: "Faris Rasyadi Putra", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Faris_MO")!, wantToLearn: "", canHelpWith: "Unity 3D Programmer"),
            JuniorExplorer(nameExpl: "Ferdinan Linardi", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Ferdinan_MO")!, wantToLearn: "Swift, UI/UX Design, Copywriting, Public Speaking, English Practice", canHelpWith: "Ilustration, Graphic Design, Fashion Styling"),
            JuniorExplorer(nameExpl: "Geraldine Janice Wibisono", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Janice_MO")!, wantToLearn: "Intermediate coding, translating design into code", canHelpWith: "Graphic design, Illustration"),
            JuniorExplorer(nameExpl: "Hana Christina Rondoh", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Hana_MO")!, wantToLearn: "XCode, Swift, UI/UX Design, English Practice, English Public Speaking, English copywriting", canHelpWith: "Fashion design, Graphic design, Fashion business and styling"),
            JuniorExplorer(nameExpl: "Henry Christanto Sutjiono", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Henry_MO")!, wantToLearn: "Basic Coding for XCode, designing prototype with Sketch", canHelpWith: "photoExplgraphy, Copywriting & Advertising"),
            JuniorExplorer(nameExpl: "Ibrahim Yunus Muhammad Fiqhan", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Fiqhan_MO")!, wantToLearn: "English, Graphic Design, Custom Component in Swift (Star Rating)", canHelpWith: "Basic programing, Object Oriented Programing (OOP), Software Design Pattern"),
            JuniorExplorer(nameExpl: "James Tirto", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Bussiness", photoExpl: UIImage(named: "James_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Janice Budihartono", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "J'Ice_MO")!, wantToLearn: "Auto layout, Intermediate Swift Code (maybe), Design", canHelpWith: "- Code\n- English"),
            JuniorExplorer(nameExpl: "Jeffrey Phinardi Kosasih", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Jep_MO")!, wantToLearn: "Public Speaking, Xcode (Constraint, Chat Feature, and Verification Code)", canHelpWith: "Youtube content, Pr, Ph, Ai, Basic coding,"),
            JuniorExplorer(nameExpl: "Jeffry Sandy Purnomo", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Jeffry_MO")!, wantToLearn: "Swift UI", canHelpWith: "Basic Code, Basic Sketch"),
            JuniorExplorer(nameExpl: "Jesslyn Faustina", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Jesslyn_MO")!, wantToLearn: "Intermediate coding", canHelpWith: "Graphic Design"),
            JuniorExplorer(nameExpl: "Josephine Sugiharto", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Jose_MO")!, wantToLearn: "Basic Coding", canHelpWith: "Branding, Graphic Design"),
            JuniorExplorer(nameExpl: "Karina Enny Agustina", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Bussiness", photoExpl: UIImage(named: "Karina_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Leonora Jeanifer", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Bussiness", photoExpl: UIImage(named: "Jean_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Lois Pangestu", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Lois_MO")!, wantToLearn: "", canHelpWith: "Basic Coding, Basic Swift, Idea"),
            JuniorExplorer(nameExpl: "M. Rizky Hidayat", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Rizky_MO")!, wantToLearn: "- English speak\n- Swift code style\n- Self management", canHelpWith: "Basic Code, SysAdmin"),
            JuniorExplorer(nameExpl: "Martin Ivo Hardinoto", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Martin_MO")!, wantToLearn: "", canHelpWith: "- Basic swift\n\n\n- Ask me everything about covid-19"),
            JuniorExplorer(nameExpl: "Michael Randicha Gunawan Santoso", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Michael_MO")!, wantToLearn: "Someone with Intermediate / Advanced understanding on TableViewController, TableViewCell, InputAccessoryView, TableViewCell's item's constraint & Protocol", canHelpWith: "Coder : Basic Swift, Intermediate Javascript (Angular & Ionic), Intermediate Laravel, Interested in Flutter"),
            JuniorExplorer(nameExpl: "Moh. Zinnur Atthufail Addausi", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "A'il_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Najibullah Ulul Albab", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Ulul_MO")!, wantToLearn: "Swift Intermediate, UX Designer", canHelpWith: "Code, UI/UX Pattern"),
            JuniorExplorer(nameExpl: "Natanael Geraldo Santoso", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Natanael_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Nicholas Ang", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Nicholas_MO")!, wantToLearn: "", canHelpWith: "Videography, Photography, Game Development (Unity), Animation and VFX, Basic Code"),
            JuniorExplorer(nameExpl: "Nur Afni Zuhrotul Laili", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Afni_MO")!, wantToLearn: "", canHelpWith: "- basic swift\n- passing data between viewcontroller\n- set onboarding screen"),
            JuniorExplorer(nameExpl: "Oktavia Citra Resmi Rachmawati", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Citra_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Rachel Audrey Effendi", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Rachel_MO")!, wantToLearn: "Basic coding", canHelpWith: "Graphic Design, Branding"),
            JuniorExplorer(nameExpl: "Rizal Hidayat", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Rizal_MO")!, wantToLearn: "English", canHelpWith: "Basic code / basic swift"),
            JuniorExplorer(nameExpl: "Siti Istiany Tribunda", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Design", photoExpl: UIImage(named: "Isti_MO")!, wantToLearn: "-English speaking\n-Public speaking\n-Team work\n- Coding, Swift, UI&UX Design.", canHelpWith: "Graphic Design :\nBranding\nLayout\n\n\nSocial Media Enthusiast :p"),
            JuniorExplorer(nameExpl: "Steven Wijaya", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "SW_MO")!, wantToLearn: "", canHelpWith: "Basic Code, Basic Swift, iOS dev, a bit MacOS dev, Laravel, Backend, etc."),
            JuniorExplorer(nameExpl: "Taufiq Ramadhany", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Taufiq_MO")!, wantToLearn: "", canHelpWith: "Basic swift"),
            JuniorExplorer(nameExpl: "Tony Varian Yoditanto", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Tony_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Wahyu Kharisma Mujiono", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Wahyujus_MO")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Wahyu Saputra", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Wahyu_MO")!, wantToLearn: "Photoeditor, English Speaking", canHelpWith: "Basic Coding"),
            JuniorExplorer(nameExpl: "Winata Arafat Fal Aham", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Tech", photoExpl: UIImage(named: "Badai_MO")!, wantToLearn: "UI/UX Design, Swift, English Speaking", canHelpWith: "Photography, Basic Coding, Critical Thinking, git,"),
            JuniorExplorer(nameExpl: "Yonatan Niko Sucahyo", levelExplorer: "Junior", shiftExpl: "Morning", roleExpl: "Bussiness", photoExpl: UIImage(named: "Niko_MO")!, wantToLearn: "basic coding, graphic Design", canHelpWith: "Business Management")
            
        ]
    }
}

class AfternoonExplorerManager {
    var arrayOfAfternoonExpl: [JuniorExplorer]
    
    init() {
        arrayOfAfternoonExpl = [
            JuniorExplorer(nameExpl: "Abraham Christopher", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Abraham_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Afitra Mamor Bikhoir", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Afitra_AF")!, wantToLearn: "how to implement crud in swift", canHelpWith: "Basic Coding"),
            JuniorExplorer(nameExpl: "Alessandro Vieri", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Aldo_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Almas Sharfina Puspandam", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Almas_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Andre Marines Ado Tena Uak", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Matu_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Angelica Irene Christina", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Bussiness", photoExpl: UIImage(named: "Ren_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Angelina Chandra", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Angel_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Arinal Haq", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Arinal_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Auriga Aristo", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Riga_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Candra Sabdana Nugroho", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Candra_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Christian Tanjono", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Christian_AF")!, wantToLearn: "- Advance OOP\n- UI Design\n- Transition on Swift (Onboarding)", canHelpWith: "- Basic Coding\n- Basic Programming Algorithm\n- Network Troubleshooting\n- Achieving your Body Goals"),
            JuniorExplorer(nameExpl: "Christopher Kevin Susandji", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Kikis_AF")!, wantToLearn: "SQLite CRUD :(((", canHelpWith: "Basic Swift, Basic Coding, Basic UI and Graphic Design, software-related stuffs (e.g. installing Minecraft, uninstalling Fortnite)"),
            JuniorExplorer(nameExpl: "David Christian Kartamihardja", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "David_AF")!, wantToLearn: "HIG, Layouting, design, UI/UX, English presentation", canHelpWith: "Basic swift, basic coding"),
            JuniorExplorer(nameExpl: "Dimarta Julkha Faradiba", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Marta_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Fauzia Umar", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Zizi_AF")!, wantToLearn: "", canHelpWith: "basic coding, basic swift, UIUX"),
            JuniorExplorer(nameExpl: "Felicia Gunadi", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Felicia_AF")!, wantToLearn: "Code, Presentation", canHelpWith: "Basic Coding, UI Design, Team Management"),
            JuniorExplorer(nameExpl: "Harvey Lienardo", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Harvey_AF")!, wantToLearn: "", canHelpWith: "Photography, image processing, layout"),
            JuniorExplorer(nameExpl: "I Putu Andhika Indrastata Widyadhana", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Putu_AF")!, wantToLearn: "Take image data from taken photos", canHelpWith: "-Basic Coding, Basic Sketch\n-Having Fun"),
            JuniorExplorer(nameExpl: "Ivan winarto", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Ivan_AF")!, wantToLearn: "present properly in english", canHelpWith: "Basic Coding\nTeam management"),
            JuniorExplorer(nameExpl: "Jeremy Endratno", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Jeremy_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Jeslyn Kosasih", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Jeslyn_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Jessica Ayumi Aris", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Ayumi_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Jessy The Wirianto", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Bussiness", photoExpl: UIImage(named: "Jessy_AF")!, wantToLearn: "Coding\nDesign\nCommunication Skill", canHelpWith: "Business Idea\nDesign Input"),
            JuniorExplorer(nameExpl: "Joahan Wirasugianto", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Joahan_AF")!, wantToLearn: "- Code\n- UI Design", canHelpWith: "Code, UI Design, Get Reletionship, Achieving your Body Goals"),
            JuniorExplorer(nameExpl: "Jovan Alvin", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Jovan_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Kevin Heryanto", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "KH_AF")!, wantToLearn: "Code, Design, HIG, English Speaking", canHelpWith: "Code, Makan-Makan."),
            JuniorExplorer(nameExpl: "Kevin Pratama Soedarso", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Kp_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Kezia Lauren Handoyo", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Kezia_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Krisna Harimurti", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Bussiness", photoExpl: UIImage(named: "Krisna_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Livia Angelica Gunawan", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Livia_AF")!, wantToLearn: "", canHelpWith: "Basic graphic design stuff (Photoshop, Illustrator), English maybe hehe, copywriting"),
            JuniorExplorer(nameExpl: "Michelle Caroline Kristanto", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Michelle_AF")!, wantToLearn: "", canHelpWith: "Graphic Design (Layouting)"),
            JuniorExplorer(nameExpl: "Moch Maulana Ardiansyah", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Maul_AF")!, wantToLearn: "", canHelpWith: "Basic Swift, UI/UX Design, Graphic Design, Layouting, Coding, Logical Thinking"),
            JuniorExplorer(nameExpl: "Monica Adelya Putri", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Monica_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Muhammad Bangun Agung", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Agung_AF")!, wantToLearn: "Presentation, Collaboration", canHelpWith: "Coding, UI Design, Animation"),
            JuniorExplorer(nameExpl: "Muhammad Faisal Febrianto", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Faisal_AF")!, wantToLearn: "ui,ux and english speaking", canHelpWith: "basic coding"),
            JuniorExplorer(nameExpl: "Muhammad Naufal Muzaky Sukmana Putra", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Zaky_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Natasha Adeline Wardhana", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Tata_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Richard Santoso", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Richard_AF")!, wantToLearn: "", canHelpWith: "basic coding, English"),
            JuniorExplorer(nameExpl: "Ricky Austin Setiabudi", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Ricky_AF")!, wantToLearn: "", canHelpWith: "Layouting, Logical Thinking, Basic Swift, photo manipulation, and pushing your score in the game"),
            JuniorExplorer(nameExpl: "Rini Handini", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Rini_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Ryan Anslyno Khohari", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Ryan_AF")!, wantToLearn: "Code with Model View", canHelpWith: "Basic Coding"),
            JuniorExplorer(nameExpl: "Samantha Teonata", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Sam_AF")!, wantToLearn: "", canHelpWith: "Graphic Design, bonding and getting along with people, ngeramein acara (BUKAN badut ultah), boosting your confidence (if you feel like you need one), ngobrol dan ngoceh"),
            JuniorExplorer(nameExpl: "Silviera Dewi Nagari", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Silviera_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Stefany", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Bussiness", photoExpl: UIImage(named: "Stefany_AF")!, wantToLearn: "pengen sih bisa gambar wkwk", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Timotius Cahyadi", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Timotius_AF")!, wantToLearn: "", canHelpWith: "Basic Coding, Basic Design"),
            JuniorExplorer(nameExpl: "Timotius Leonardo Lianoto", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Timol_AF")!, wantToLearn: "Design and how to pick a good colour collaboration", canHelpWith: "Code Logic, Public Speaking"),
            JuniorExplorer(nameExpl: "Tivany Hartono", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Design", photoExpl: UIImage(named: "Tivany_AF")!, wantToLearn: "", canHelpWith: "Graphic Design"),
            JuniorExplorer(nameExpl: "Ulinnuha Nabilah", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Nabilah_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "Victor Wijaya Ngantung", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Bussiness", photoExpl: UIImage(named: "Victor_AF")!, wantToLearn: "Coding", canHelpWith: "Business\nDesign Basics"),
            JuniorExplorer(nameExpl: "Wienona Martha Parlina", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "Wienona_AF")!, wantToLearn: "", canHelpWith: ""),
            JuniorExplorer(nameExpl: "William Sebastian Thedja", levelExplorer: "Junior", shiftExpl: "Afternoon", roleExpl: "Tech", photoExpl: UIImage(named: "William_AF")!, wantToLearn: "", canHelpWith: "")
        ]
    }
}
