//
//  SeniorExplorer.swift
//  Challenge2
//
//  Created by Lois Pangestu on 15/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import Foundation
import UIKit

struct SeniorExplorer {
    
    var nameExpl : String
    var levelExplorer : String
    var shiftExpl : String
    var roleExpl : String
    var photoExpl : UIImage
    var wantToLearnExpl : String
    var canHelpWithExpl : String
}

class SeniorExplorerManager {
    var arrayOfSeniorExpl: [SeniorExplorer]
    
    init() {
        arrayOfSeniorExpl = [
            SeniorExplorer(nameExpl: "Ari Kurniawan", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Design Facilitator", photoExpl: UIImage(named: "Ari_SE")!, wantToLearnExpl: "Basic coding", canHelpWithExpl: "Graphic Design (layout/composition, typography, branding), HIG, UI/UX"),
            SeniorExplorer(nameExpl: "Bayu Prasetya", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Design Facilitator", photoExpl: UIImage(named: "Bayu_SE")!, wantToLearnExpl: "", canHelpWithExpl: "Visual Branding, Design Thinking, Development Process, UI/UX, Nemenin Mikir, Makan-makan (soon)"),
            SeniorExplorer(nameExpl: "Dickson Leonard", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Academy Manager", photoExpl: UIImage(named: "Dickson_SE")!, wantToLearnExpl: "", canHelpWithExpl: "Coding basics (algorithm, data structure, operations)\nApp development process (problem definition, ideation, prototyping, production), Basic UX, Professional skills (leadership, communication)"),
            SeniorExplorer(nameExpl: "Fanny Halim", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Coding Facilitator", photoExpl: UIImage(named: "Fanny_SE")!, wantToLearnExpl: "English Speaking  uwu", canHelpWithExpl: "Basic iOS Development\n(Swift, XCode, Auto Layout, Data Collection)\nDiscussion"),
            SeniorExplorer(nameExpl: "Gabriele Wijasa", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Design Facilitator", photoExpl: UIImage(named: "Gabriele_SE")!, wantToLearnExpl: "", canHelpWithExpl: "Graphic Design, UI/UX, Photography"),
            SeniorExplorer(nameExpl: "Januar Tanzil", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Coding Facilitator", photoExpl: UIImage(named: "Januar_SE")!, wantToLearnExpl: "", canHelpWithExpl: "Swift language, IOS Development, Game Development, Software Engineering in general"),
            SeniorExplorer(nameExpl: "Jaya Pranata", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Coding Facilitator", photoExpl: UIImage(named: "Jaya_SE")!, wantToLearnExpl: "Graphic Design (layout/composition, typography, branding), UI/UX, problem analysis, basic research, HIG", canHelpWithExpl: "Coding basic, Swift Language, Auto Layout, Core Data, Map"),
            SeniorExplorer(nameExpl: "John Alan Ketaren", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Professional Facilitator", photoExpl: UIImage(named: "Ketaren_SE")!, wantToLearnExpl: "Basic Xcode", canHelpWithExpl: "Ideation, bringing resources, user testing, business, fintech, Presentation,Networking."),
            SeniorExplorer(nameExpl: "Rachmat Kukuh", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Coding Facilitator", photoExpl: UIImage(named: "Kukuh_SE")!, wantToLearnExpl: "", canHelpWithExpl: "- General iOS app development\n- iOS frameworks\n- Swift language"),
            SeniorExplorer(nameExpl: "Yehezkiel Cheryan", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Professional Facilitator", photoExpl: UIImage(named: "Ryan_SE")!, wantToLearnExpl: "Swift/Xcode in general", canHelpWithExpl: "English language, Public speaking, Business presentation, Business writing, Business insights"),
            SeniorExplorer(nameExpl: "Yulibar Husni", levelExplorer: "Senior", shiftExpl: "All", roleExpl: "Coding Facilitator", photoExpl: UIImage(named: "Yulibar_SE")!, wantToLearnExpl: "- Ideation\n- English Speaking\n- Project Management", canHelpWithExpl: "- Swift \n- Ideation\n- Animation production")
        ]
    }
}
