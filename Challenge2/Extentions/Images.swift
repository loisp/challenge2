//
//  JourneyImages.swift
//  Challenge2
//
//  Created by Lois Pangestu on 12/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

struct JourneyImages {
    static let challenge1 = UIImage(named: "Challenge/challenge1")!
    static let challenge2 = UIImage(named: "Challenge/challenge2")!
    static let challenge3 = UIImage(named: "Challenge/challenge3")!
    static let challenge4 = UIImage(named: "Challenge/challenge4")!
    static let challenge5 = UIImage(named: "Challenge/challenge5")!
    static let challenge6 = UIImage(named: "Challenge/challenge6")!
}
