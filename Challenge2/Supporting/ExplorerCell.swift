//
//  SeniorCell.swift
//  Challenge2
//
//  Created by Lois Pangestu on 15/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit
class ExplorerCell: UICollectionViewCell{
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var imageExplorer: UIImageView!
    @IBOutlet weak var nameExplorer: UILabel!
    @IBOutlet weak var roleExplorer: UILabel!
    
    func setSeniorExplorer(seniorExplorer: SeniorExplorer){
        
        //Prepare data & View
        imageExplorer.layer.cornerRadius = 15
        imageExplorer.layer.masksToBounds = true

        let namaExp: String = seniorExplorer.nameExpl

        let styleAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 20)
        ]

        let attributedString = NSAttributedString(string: namaExp, attributes: styleAttributes)

        //Set data to view
        imageExplorer.image             = seniorExplorer.photoExpl
        nameExplorer.attributedText     = attributedString
        roleExplorer.text               = seniorExplorer.roleExpl
    }
    
    func setMorningExplorer(morningExplorer: JuniorExplorer){
        
        //Prepare data & View
        imageExplorer.layer.cornerRadius = 15
        imageExplorer.layer.masksToBounds = true
        
        let namaExp: String = morningExplorer.nameExpl

        let styleAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 20)
        ]

        let attributedString = NSAttributedString(string: namaExp, attributes: styleAttributes)

        //Set data to view
        imageExplorer.image             = morningExplorer.photoExpl
        nameExplorer.attributedText     = attributedString
        roleExplorer.text               = morningExplorer.roleExpl
    }
    
    func setAfternoonExplorer(afternoonExplorer: JuniorExplorer){
        
        //Prepare data & View
        imageExplorer.layer.cornerRadius = 15
        imageExplorer.layer.masksToBounds = true
        
        let namaExp: String = afternoonExplorer.nameExpl

        let styleAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 20)
        ]

        let attributedString = NSAttributedString(string: namaExp, attributes: styleAttributes)

        //Set data to view
        imageExplorer.image             = afternoonExplorer.photoExpl
        nameExplorer.attributedText     = attributedString
        roleExplorer.text               = afternoonExplorer.roleExpl
    }

}
