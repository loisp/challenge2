//
//  AllExplorerCell.swift
//  Challenge2
//
//  Created by Lois Pangestu on 14/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

class AllExplorerCell: UICollectionViewCell{
    
    @IBOutlet weak var cardview: UIView!
    @IBOutlet weak var explorerImg: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var role: UILabel!
    
    func setExplorer(journey: JourneyDetail){
        
        //Prepare data & View
        let nama: String = journey.title
        
        let styleAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 20)
        ]
        
        let attributedString = NSAttributedString(string: nama, attributes: styleAttributes)
        
        cardview.layer.cornerRadius = 15
        
        //Set data to view
        explorerImg.image       = journey.image
        name.text               = nama
        name.attributedText     = attributedString
        role.text               = journey.category
    }

}
