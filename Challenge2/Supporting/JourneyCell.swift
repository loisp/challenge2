//
//  JourneyCellTableViewCell.swift
//  Challenge2
//
//  Created by Lois Pangestu on 12/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

class JourneyCell: UITableViewCell {
    
    @IBOutlet weak var cardview: UIView!
    @IBOutlet weak var journeyImg: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var duration: UILabel!
    
    func setJourney(journey: JourneyDetail){
        
        cardview.layer.cornerRadius = 15
        cardview.layer.masksToBounds = true
        
        journeyImg.image    = journey.image
        title.text          = journey.title
        category.text       = journey.category
        duration.text       = journey.duration + " Weeks"
    }

}
