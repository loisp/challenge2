//
//  ViewController.swift
//  Challenge2
//
//  Created by Lois Pangestu on 10/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    //Outlet
    @IBOutlet weak var journeyTableView: UITableView!
    
    // Other Variable
    var arrayOfJourneyDetails: [JourneyDetail]!
    var journeyIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //Change text Status bar color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Change status bar
        navigationController?.navigationBar.barStyle = .black
    }
}


extension MainViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfJourneyDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get the data to show 1-1
        let journey = arrayOfJourneyDetails[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JourneyCell") as! JourneyCell
        
        cell.setJourney(journey: journey)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       tableView.deselectRow(at: indexPath, animated: true)

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailJourney = storyboard.instantiateViewController(withIdentifier: "DetailJourney") as! DetailJourneyVC
        //detailJourney.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        detailJourney.hidesBottomBarWhenPushed = true

        detailJourney.arrayOfJourneyDetail = arrayOfJourneyDetails[indexPath.row]
        self.navigationController?.pushViewController(detailJourney, animated: true)
        //Send arrayOfJourneys[indexPath.row] to next screen

//        detailJourney.imageJourney.image = arrayOfJourneys[indexPath.row].image
//        detailJourney.titleJourney.text = arrayOfJourneys[indexPath.row].title
//        detailJourney.categoryJourney.text = arrayOfJourneys[indexPath.row].category
//        detailJourney.durationJourney.text = arrayOfJourneys[indexPath.row].duration
//        detailJourney.dataImageView.image = arrayOfItems[indexPath.row].image

    }
}
