//
//  TableViewController.swift
//  Challenge2
//
//  Created by Lois Pangestu on 16/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    //    var arrayOfJourneyDetails: [JourneyDetail]!
    var arrayOfSeniorExpl: [SeniorExplorer]!
    
    var journeyIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Explorers"
        

        self.view.addSubview(collectionView)
    }
    
    // Change text status bar color
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Change status bar
        navigationController?.navigationBar.barStyle = .black
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
}
