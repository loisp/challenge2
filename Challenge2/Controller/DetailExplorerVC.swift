//
//  DetailExplorerVC.swift
//  Challenge2
//
//  Created by Lois Pangestu on 15/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

class DetailExplorerVC: UIViewController {

    @IBOutlet weak var explorerImg: UIImageView!
    @IBOutlet weak var cardView1: UIView!
    @IBOutlet weak var cardView2: UIView!
    @IBOutlet weak var cardView3: UIView!
    
    @IBOutlet weak var namaExplr: UILabel!
    @IBOutlet weak var roleExplr: UILabel!
    @IBOutlet weak var shiftExplr: UILabel!
    @IBOutlet weak var levelExplr: UILabel!
    @IBOutlet weak var canHelpExlpr: UILabel!
    @IBOutlet weak var canLearnExplr: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrayOfSeniorExpl: SeniorExplorer!
    var arrayOfMorningExpl: JuniorExplorer!
    var arrayOfAfternoonExpl: JuniorExplorer!
    
    var journeyIndex = 0
    
    var arrayDataOfSeniorExpl: [SeniorExplorer]!
    var arrayDataOfMorningExpl: [JuniorExplorer]!
    var arrayDataOfAfternoonExpl: [JuniorExplorer]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setView()
        
        title = "Explorer's Profile"
        
        // Check which data
        if arrayOfSeniorExpl != nil {
            explorerImg.image = arrayOfSeniorExpl.photoExpl
            namaExplr.text = arrayOfSeniorExpl.nameExpl
            roleExplr.text = arrayOfSeniorExpl.roleExpl
            shiftExplr.text = arrayOfSeniorExpl.shiftExpl + " Shift"
            levelExplr.text = arrayOfSeniorExpl.levelExplorer + " Explorer"
            canHelpExlpr.text = arrayOfSeniorExpl.canHelpWithExpl
            canLearnExplr.text = arrayOfSeniorExpl.wantToLearnExpl
            
            if canHelpExlpr.text == "" {
                canHelpExlpr.text = "-"
            }
            if canLearnExplr.text == "" {
                canLearnExplr.text = "-"
            }
        }
        else if arrayOfMorningExpl != nil {
            explorerImg.image = arrayOfMorningExpl.photoExpl
            namaExplr.text = arrayOfMorningExpl.nameExpl
            roleExplr.text = arrayOfMorningExpl.roleExpl
            shiftExplr.text = arrayOfMorningExpl.shiftExpl + " Shift"
            levelExplr.text = arrayOfMorningExpl.levelExplorer + " Explorer"
            canHelpExlpr.text = arrayOfMorningExpl.canHelpWith
            canLearnExplr.text = arrayOfMorningExpl.wantToLearn

            if canHelpExlpr.text == "" {
                canHelpExlpr.text = "-"
            }
            if canLearnExplr.text == "" {
                canLearnExplr.text = "-"
            }
        }
        else if arrayOfAfternoonExpl != nil {
            explorerImg.image = arrayOfAfternoonExpl.photoExpl
            namaExplr.text = arrayOfAfternoonExpl.nameExpl
            roleExplr.text = arrayOfAfternoonExpl.roleExpl
            shiftExplr.text = arrayOfAfternoonExpl.shiftExpl + " Shift"
            levelExplr.text = arrayOfAfternoonExpl.levelExplorer + " Explorer"
            canHelpExlpr.text = arrayOfAfternoonExpl.canHelpWith
            canLearnExplr.text = arrayOfAfternoonExpl.wantToLearn
            
            if canHelpExlpr.text == "" {
                canHelpExlpr.text = "-"
            }
            if canLearnExplr.text == "" {
                canLearnExplr.text = "-"
            }
        }
    }
    
    func setView(){
        explorerImg.layer.cornerRadius = 15.0
        explorerImg.layer.masksToBounds = true
        cardView1.layer.cornerRadius = 15.0
        cardView1.layer.masksToBounds = true
        cardView2.layer.cornerRadius = 15.0
        cardView2.layer.masksToBounds = true
        cardView3.layer.cornerRadius = 15.0
        cardView3.layer.masksToBounds = true
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.prefersLargeTitles = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
}

extension DetailExplorerVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        var index: Int = 0
        
//        if arrayDataOfSeniorExpl.contains("Design Facilitator"){
//            print(arrayDataOfSeniorExpl[index].nameExpl)
//        }
        
//        return self.all().filter {
//            explorer in explorer.shiftExpl == "All"
//        }
        
//        return SeniorExplorerManager().arrayOfSeniorExpl.filter{
//            explorer in SeniorExplorer.role == "Design Facilitator"
//        }
//            {
//                explorer in explorer.role == "Tech"
//        }
        
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if arrayOfSeniorExpl != nil {
            let similarExplr = arrayDataOfSeniorExpl[indexPath.row]
            
//            if similarExplr roleExpl == "Coding Facilitator" {
//                print("Design Facilitator : ", similarExplr.nameExpl)
                
                let cellSimilarRole = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarRole", for: indexPath) as! ExplorerCell

                cellSimilarRole.setSeniorExplorer(seniorExplorer: similarExplr)

                return cellSimilarRole
//            }
        } else{
            if arrayOfMorningExpl != nil {

                let similarExplr = arrayDataOfMorningExpl[indexPath.row]

                let cellSimilarRole = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarRole", for: indexPath) as! ExplorerCell

                cellSimilarRole.setMorningExplorer(morningExplorer: similarExplr)

                return cellSimilarRole
            }
            else if arrayOfAfternoonExpl != nil {
                let similarExplr = arrayDataOfAfternoonExpl[indexPath.row]

                let cellSimilarRole = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarRole", for: indexPath) as! ExplorerCell

                cellSimilarRole.setAfternoonExplorer(afternoonExplorer: similarExplr)

                return cellSimilarRole
            }
        }

        return UICollectionViewCell()
    }
    
    // Set ukuran cell (HARUS PAKAI)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 185, height: 245)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let detailExplr = storyboard.instantiateViewController(withIdentifier: "DetailExplorer") as! DetailExplorerVC
//        detailExplr.hidesBottomBarWhenPushed = true

        if arrayOfSeniorExpl != nil {
            // Open to new screen
//            detailExplr.arrayOfSeniorExpl = arrayOfSeniorExpl
//            detailExplr.arrayDataOfSeniorExpl = arrayDataOfSeniorExpl
//
//            self.navigationController?.pushViewController(detailExplr, animated: true)
            
            explorerImg.image       = arrayDataOfSeniorExpl[indexPath.row].photoExpl
            namaExplr.text          = arrayDataOfSeniorExpl[indexPath.row].nameExpl
            roleExplr.text          = arrayDataOfSeniorExpl[indexPath.row].roleExpl
            shiftExplr.text         = arrayDataOfSeniorExpl[indexPath.row].shiftExpl + " Shift"
            levelExplr.text         = arrayDataOfSeniorExpl[indexPath.row].levelExplorer + " Explorer"
            canHelpExlpr.text       = arrayDataOfSeniorExpl[indexPath.row].canHelpWithExpl
            canLearnExplr.text      = arrayDataOfSeniorExpl[indexPath.row].wantToLearnExpl

            if canHelpExlpr.text == "" {
                canHelpExlpr.text = "-"
            }
            if canLearnExplr.text == "" {
                canLearnExplr.text = "-"
            }
        }
        else if arrayOfMorningExpl != nil {
            // Open to new screen
//            detailExplr.arrayOfMorningExpl = arrayOfMorningExpl
//            detailExplr.arrayDataOfMorningExpl = arrayDataOfMorningExpl
//
//            self.navigationController?.pushViewController(detailExplr, animated: true)
            
            explorerImg.image       = arrayDataOfMorningExpl[indexPath.row].photoExpl
            namaExplr.text          = arrayDataOfMorningExpl[indexPath.row].nameExpl
            roleExplr.text          = arrayDataOfMorningExpl[indexPath.row].roleExpl
            shiftExplr.text         = arrayDataOfMorningExpl[indexPath.row].shiftExpl + " Shift"
            levelExplr.text         = arrayDataOfMorningExpl[indexPath.row].levelExplorer + " Explorer"
            canHelpExlpr.text       = arrayDataOfMorningExpl[indexPath.row].canHelpWith
            canLearnExplr.text      = arrayDataOfMorningExpl[indexPath.row].wantToLearn

            if canHelpExlpr.text == "" {
                canHelpExlpr.text = "-"
            }
            if canLearnExplr.text == "" {
                canLearnExplr.text = "-"
            }
        }
        else if arrayOfAfternoonExpl != nil {
            // Open to new screen
//            detailExplr.arrayOfAfternoonExpl = arrayOfAfternoonExpl
//            detailExplr.arrayDataOfAfternoonExpl = arrayDataOfAfternoonExpl
//
//            self.navigationController?.pushViewController(detailExplr, animated: true)
            
            
            
            explorerImg.image       = arrayDataOfAfternoonExpl[indexPath.row].photoExpl
            namaExplr.text          = arrayDataOfAfternoonExpl[indexPath.row].nameExpl
            roleExplr.text          = arrayDataOfAfternoonExpl[indexPath.row].roleExpl
            shiftExplr.text         = arrayDataOfAfternoonExpl[indexPath.row].shiftExpl + " Shift"
            levelExplr.text         = arrayDataOfAfternoonExpl[indexPath.row].levelExplorer + " Explorer"
            canHelpExlpr.text       = arrayDataOfAfternoonExpl[indexPath.row].canHelpWith
            canLearnExplr.text      = arrayDataOfAfternoonExpl[indexPath.row].wantToLearn

            if canHelpExlpr.text == "" {
                canHelpExlpr.text = "-"
            }
            if canLearnExplr.text == "" {
                canLearnExplr.text = "-"
            }
        }
    }
}
