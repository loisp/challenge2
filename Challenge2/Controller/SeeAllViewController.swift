//
//  SeeAllViewController.swift
//  Challenge2
//
//  Created by Lois Pangestu on 20/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

class SeeAllViewController: UIViewController {
    
    var contentType: String!

    @IBOutlet weak var collectionView: UICollectionView!
    
    var arrayOfSeniorExpl: [SeniorExplorer]!
    var arrayOfMorningExpl: [JuniorExplorer]!
    var arrayOfAfternoonExpl: [JuniorExplorer]!
//    var arrayOfAfternoonExpl: [AfternoonExplorer]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = contentType
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let width = view.frame.size.width
        
        let cellSize = CGSize(width:width * 0.45 , height: 245)

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 10, bottom: 1, right: 10)
        
        collectionView.setCollectionViewLayout(layout, animated: true)

        collectionView.reloadData()
    }
}

extension SeeAllViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if contentType == "Senior Explorers" {
            return arrayOfSeniorExpl.count
        }
        else if contentType == "Morning Explorers" {
            return arrayOfMorningExpl.count
        }
        else if contentType == "Afternoon Explorers" {
            return arrayOfAfternoonExpl.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeeAllCell", for: indexPath) as! ExplorerCell
        
        if contentType == "Senior Explorers" {
            //Senior Collection View
            let seniorExplr = arrayOfSeniorExpl[indexPath.row]

            collectionCell.setSeniorExplorer(seniorExplorer: seniorExplr)
            return collectionCell
        }
        else if contentType == "Morning Explorers" {
            //Senior Collection View
            let morningExplr = arrayOfMorningExpl[indexPath.row]
            
            collectionCell.setMorningExplorer(morningExplorer: morningExplr)
            return collectionCell
        }
        else if contentType == "Afternoon Explorers" {
            //Senior Collection View
            let afternoonExplr = arrayOfAfternoonExpl[indexPath.row]
            
            collectionCell.setAfternoonExplorer(afternoonExplorer: afternoonExplr)
            return collectionCell
        }
        
        return UICollectionViewCell()
    }
    
    // Set ukuran cell (HARUS PAKAI)
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
////        return CGSize(width: 5, height: 245)
////        return CGSize(width: collectionView.bounds.width / 20 , height: collectionView.bounds.height / 1.8)
//        let height = view.frame.size.height
//        let width = view.frame.size.width
//        // in case you you want the cell to be 40% of your controllers view
////        return CGSize(width: width * 0.2, height: height * 0.4)
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailExplr = storyboard.instantiateViewController(withIdentifier: "DetailExplorer") as! DetailExplorerVC
        detailExplr.hidesBottomBarWhenPushed = true
        
        if contentType == "Senior Explorers" {
            
            detailExplr.arrayOfSeniorExpl = arrayOfSeniorExpl[indexPath.row]
            detailExplr.arrayDataOfSeniorExpl = arrayOfSeniorExpl
            
            self.navigationController?.pushViewController(detailExplr, animated: true)
        }
        else if contentType == "Morning Explorers" {
            
            detailExplr.arrayOfMorningExpl = arrayOfMorningExpl[indexPath.row]
            detailExplr.arrayDataOfMorningExpl = arrayOfMorningExpl
            
            self.navigationController?.pushViewController(detailExplr, animated: true)
        }
        else if contentType == "Afternoon Explorers" {
            
            detailExplr.arrayOfAfternoonExpl = arrayOfAfternoonExpl[indexPath.row]
            detailExplr.arrayDataOfAfternoonExpl = arrayOfAfternoonExpl
            
            self.navigationController?.pushViewController(detailExplr, animated: true)
        }
    }
}
