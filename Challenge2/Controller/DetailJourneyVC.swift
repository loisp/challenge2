//
//  DetailJourneyVC.swift
//  Challenge2
//
//  Created by Lois Pangestu on 12/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

class DetailJourneyVC: UIViewController {

    @IBOutlet weak var imageJourney: UIImageView!
    @IBOutlet weak var titleJourney: UILabel!
    @IBOutlet weak var descriptionJourney: UITextView!
    
    @IBOutlet weak var typeJourney: UILabel!
    @IBOutlet weak var categoryJourney: UILabel!
    @IBOutlet weak var durationJourney: UILabel!
    @IBOutlet weak var goalsLabel: UILabel!
    @IBOutlet weak var constraintsLabel: UILabel!
    @IBOutlet weak var deliverablesLabel: UILabel!
    @IBOutlet weak var backgroundGradient: UIView!
    
    
    @IBOutlet weak var cardView1: UIView!
    @IBOutlet weak var cardView2: UIView!
    @IBOutlet weak var cardView3: UIView!
    @IBOutlet weak var cardView4: UIView!
    
    var arrayOfJourneyDetail: JourneyDetail!
    var journeyIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Init
        backgroundGradient.setGradientBackground()
        imageJourney.image = arrayOfJourneyDetail.image
        titleJourney.text = arrayOfJourneyDetail.title
        descriptionJourney.text = arrayOfJourneyDetail.description
        typeJourney.text = arrayOfJourneyDetail.type
        categoryJourney.text = arrayOfJourneyDetail.category
        durationJourney.text = arrayOfJourneyDetail.duration

        goalsLabel.text = arrayOfJourneyDetail.goals.map {String($0)}.joined(separator: ",")
        constraintsLabel.text = arrayOfJourneyDetail.constraints.map {String($0)}.joined(separator: ",")
        deliverablesLabel.text = arrayOfJourneyDetail.deliverables.map {String($0)}.joined(separator: ",")
        
        cardView1.layer.cornerRadius = 15.0
        cardView1.layer.masksToBounds = true
        cardView2.layer.cornerRadius = 15.0
        cardView2.layer.masksToBounds = true
        cardView3.layer.cornerRadius = 15.0
        cardView3.layer.masksToBounds = true
        cardView4.layer.cornerRadius = 15.0
        cardView4.layer.masksToBounds = true
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    //Change text Status bar color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        navigationController?.navigationBar.barStyle = .black
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // ACTION
    @IBAction func clickBackBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}
extension UIView {
    func setGradientBackground(){
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [UIColor.black.cgColor, "1E1E1E"]
        gradientLayer.locations = [0.03, 0.5]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
        
    }
}
