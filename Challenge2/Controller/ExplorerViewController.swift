//
//  ExplorerViewController.swift
//  Challenge2
//
//  Created by Lois Pangestu on 14/04/20.
//  Copyright © 2020 Lois Pangestu. All rights reserved.
//

import UIKit

class ExplorerViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var collectionView3: UICollectionView!
    
    var arrayOfSeniorExpl: [SeniorExplorer]!
    var arrayOfMorningExpl: [JuniorExplorer]!
    var arrayOfAfternoonExpl: [JuniorExplorer]!
    
    var journeyIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Explorers"
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView2.delegate = self
        collectionView2.dataSource = self
        collectionView3.delegate = self
        collectionView3.dataSource = self
    }
    
    // Change text status bar color
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Change status bar
        navigationController?.navigationBar.barStyle = .black
    }
    
    // ACTION
    @IBAction func seeAllSeniorBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let seeAllStory = storyboard.instantiateViewController(withIdentifier: "SeeAllExplorer") as! SeeAllViewController
        seeAllStory.hidesBottomBarWhenPushed = true

        seeAllStory.contentType = "Senior Explorers"
        seeAllStory.arrayOfSeniorExpl = arrayOfSeniorExpl
        
        self.navigationController?.pushViewController(seeAllStory, animated: true)
    }
    
    @IBAction func seeAllMorningBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let seeAllStory = storyboard.instantiateViewController(withIdentifier: "SeeAllExplorer") as! SeeAllViewController
        seeAllStory.hidesBottomBarWhenPushed = true

        seeAllStory.contentType = "Morning Explorers"
        seeAllStory.arrayOfMorningExpl = arrayOfMorningExpl
        self.navigationController?.pushViewController(seeAllStory, animated: true)
    }
    
    @IBAction func seeAllAfternoonBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let seeAllStory = storyboard.instantiateViewController(withIdentifier: "SeeAllExplorer") as! SeeAllViewController
        seeAllStory.hidesBottomBarWhenPushed = true

        seeAllStory.contentType = "Afternoon Explorers"
        seeAllStory.arrayOfAfternoonExpl = arrayOfAfternoonExpl
        
        self.navigationController?.pushViewController(seeAllStory, animated: true)
    }
}

extension ExplorerViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("Masuk 1 : ", arrayOfSeniorExpl.count)
        if collectionView == self.collectionView{
            return arrayOfSeniorExpl.count
        }
        else if collectionView == self.collectionView2{
            return arrayOfMorningExpl.count
        }
        else if collectionView == self.collectionView3{
            return arrayOfAfternoonExpl.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        print("Masuk 2 : ", arrayOfSeniorExpl[indexPath.row].nameExpl)
        
        if collectionView == self.collectionView{
            //Senior Collection View
            let seniorExplr = arrayOfSeniorExpl[indexPath.row]

            let cellSenior = collectionView.dequeueReusableCell(withReuseIdentifier: "ExplorerCell", for: indexPath) as! ExplorerCell

            cellSenior.setSeniorExplorer(seniorExplorer: seniorExplr)

            return cellSenior
        }
        else if collectionView == self.collectionView2{
            //Morning Collection View
            let morningExplr = arrayOfMorningExpl[indexPath.row]

            let cellMorning = collectionView.dequeueReusableCell(withReuseIdentifier: "MorningCell", for: indexPath) as! ExplorerCell

            cellMorning.setMorningExplorer(morningExplorer: morningExplr)

            return cellMorning
        }
        else if collectionView == self.collectionView3{
            //Morning Collection View
            let afternoonExplr = arrayOfAfternoonExpl[indexPath.row]

            let cellAfternoon = collectionView.dequeueReusableCell(withReuseIdentifier: "AfternoonCell", for: indexPath) as! ExplorerCell

            cellAfternoon.setAfternoonExplorer(afternoonExplorer: afternoonExplr)

            return cellAfternoon
        }
        return UICollectionViewCell()
    }
    
    // Set ukuran cell (HARUS PAKAI)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("Masuk 3")
        return CGSize(width: 185, height: 245)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if collectionView == self.collectionView {
            let detailExplr = storyboard.instantiateViewController(withIdentifier: "DetailExplorer") as! DetailExplorerVC
            detailExplr.hidesBottomBarWhenPushed = true

            detailExplr.arrayOfSeniorExpl = arrayOfSeniorExpl[indexPath.row]
            detailExplr.arrayDataOfSeniorExpl = arrayOfSeniorExpl
            self.navigationController?.pushViewController(detailExplr, animated: true)
        }
        else if collectionView == self.collectionView2 {
            let detailExplr = storyboard.instantiateViewController(withIdentifier: "DetailExplorer") as! DetailExplorerVC
            detailExplr.hidesBottomBarWhenPushed = true

            detailExplr.arrayOfMorningExpl = arrayOfMorningExpl[indexPath.row]
            detailExplr.arrayDataOfMorningExpl = arrayOfMorningExpl
            self.navigationController?.pushViewController(detailExplr, animated: true)
        }
        else if collectionView == self.collectionView3 {
            let detailExplr = storyboard.instantiateViewController(withIdentifier: "DetailExplorer") as! DetailExplorerVC
            detailExplr.hidesBottomBarWhenPushed = true

            detailExplr.arrayOfAfternoonExpl = arrayOfAfternoonExpl[indexPath.row]
            detailExplr.arrayDataOfAfternoonExpl = arrayOfAfternoonExpl
            
            self.navigationController?.pushViewController(detailExplr, animated: true)
        }
    }
}
